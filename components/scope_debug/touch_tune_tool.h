#pragma once

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    SYSTEM_UART_PORT = 0,
    USER_UART_PORT,
    MAX_UART_PORT         //reserve
} frame_push_type_t;

typedef enum {
    MODE_115200 = 115200,
    MODE_9600   = 9600,
    MODE_MAX        //reserve
} frame_baud_rate_type_t;

typedef struct
{
    const touch_pad_t *button;
    const int channal_cnt;
    frame_push_type_t frame_push_type;
    int touch_tune_uart_num;
    int touch_tune_tx_pin;
    int touch_tune_rx_pin;
    frame_baud_rate_type_t touch_tune_baud_rate;
} touch_tune_config_t;


/*
If you select the system uart port as the port of pushing data port(frame_push_type = SYSTEM_UART_PORT),
you don't need to initialize the uart_num and baud rate, as well as the tx && rx pin.
*/
esp_err_t touch_tune_tool_init(touch_tune_config_t * touch_tune_config);

esp_err_t touch_tune_tool_write(void);


#ifdef __cplusplus
}
#endif