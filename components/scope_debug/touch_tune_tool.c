#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "driver/uart.h"
#include "driver/gpio.h"
#include "driver/touch_pad.h"
#include "esp_log.h"

#include "touch_tune_tool.h"


#define TAG "touch_tune_tool"

#define BUF_SIZE                (1024)
#define CHANNEL_MSG_SIZE        (50)


#define TOUCH_TUNE_CHECK(a, str, ret_val) \
    if(!(a)) { \
        ESP_LOGE(TAG,"%s(%d): %s", __FUNCTION__, __LINE__, str); \
        return (ret_val); \
    }

#define touch_tune_push_msg(push_uart_num, str, msglen) \
    if((push_uart_num) == UART_NUM_MAX) { \
        printf(str); \
    } else { \
        uart_write_bytes(push_uart_num, (const char *)str, msglen); \
    }




#if CONFIG_IDF_TARGET_ESP32
typedef struct
{
    touch_pad_t touch_num;
    uint16_t raw_val;
    uint16_t filter_val;
} touch_data_t;

typedef esp_err_t (*fn_read_raw)(touch_pad_t, uint16_t *);
typedef esp_err_t (*fn_read_filtered)(touch_pad_t, uint16_t *);

#elif CONFIG_IDF_TARGET_ESP32S2BETA
typedef struct
{
    touch_pad_t touch_num;
    uint32_t raw;
    uint32_t baseline;
} touch_data_t;
#endif


typedef struct
{
    int channal_cnt;
    int touch_tune_uart_num;
    char* framemsg;
    touch_pad_t *button;
    touch_data_t *touch_data_list;
    fn_read_raw read_raw;
    fn_read_filtered read_filtered;
} touch_tune_obj_t;
touch_tune_obj_t *p_touch_tune_obj;


esp_err_t touch_tune_tool_init(touch_tune_config_t * touch_tune_config)
{
    TOUCH_TUNE_CHECK((touch_tune_config->touch_tune_baud_rate != MODE_115200) && (touch_tune_config->touch_tune_baud_rate != MODE_9600), 
                        "touch tune tool don't support this baud rate", ESP_ERR_INVALID_ARG);
    TOUCH_TUNE_CHECK((touch_tune_config->frame_push_type < MAX_UART_PORT), "frame push type error", ESP_ERR_INVALID_ARG);
    
    p_touch_tune_obj = (touch_tune_obj_t *)malloc(sizeof(touch_tune_obj_t));
    p_touch_tune_obj->touch_data_list = (touch_data_t *)malloc(sizeof(touch_data_t) * touch_tune_config->channal_cnt);
    p_touch_tune_obj->framemsg = (char *)malloc(sizeof(char) * CHANNEL_MSG_SIZE * touch_tune_config->channal_cnt);
    if(p_touch_tune_obj == NULL || p_touch_tune_obj->framemsg == NULL || p_touch_tune_obj->touch_data_list ==NULL) {
        ESP_LOGW(TAG, "Have not enough space, heap free: %d", esp_get_free_heap_size());
        return ESP_ERR_NO_MEM;
    }

    if(touch_tune_config->frame_push_type != SYSTEM_UART_PORT) {
        uart_config_t uart_config = {
            .baud_rate  = touch_tune_config->touch_tune_baud_rate,
            .data_bits  = UART_DATA_8_BITS,
            .parity     = UART_PARITY_DISABLE,
            .stop_bits  = UART_STOP_BITS_1,
            .flow_ctrl  = UART_HW_FLOWCTRL_DISABLE,
            .source_clk = UART_SCLK_APB,
        };
        uart_driver_install(touch_tune_config->touch_tune_uart_num, BUF_SIZE * 2, 0, 0, NULL, 0);
        uart_param_config(touch_tune_config->touch_tune_uart_num, &uart_config);
        uart_set_pin(touch_tune_config->touch_tune_uart_num, touch_tune_config->touch_tune_tx_pin,
                                                    touch_tune_config->touch_tune_rx_pin, -1, -1);
        p_touch_tune_obj->touch_tune_uart_num = touch_tune_config->touch_tune_uart_num;
    } else {
        p_touch_tune_obj->touch_tune_uart_num = UART_NUM_MAX;
    }
    p_touch_tune_obj->button = touch_tune_config->button;
    p_touch_tune_obj->channal_cnt = touch_tune_config->channal_cnt;
    p_touch_tune_obj->read_raw = touch_pad_read;
    p_touch_tune_obj->read_filtered = touch_pad_read_filtered;    
    return ESP_OK;
}

inline int make_frame(touch_data_t *touch_frame_buf, int channel_cnt, char* frame_msg_buf)
{
    int len = 1;
    memset(frame_msg_buf, '\0', sizeof(char) * 50 * channel_cnt);
    frame_msg_buf[0] = '{';
    for(int cnt = 0; cnt < channel_cnt; cnt++) {
        len += sprintf(frame_msg_buf + len,"\"channel_%d\":{\"raw\":%d,\"base\":%d},",\
            touch_frame_buf[cnt].touch_num, touch_frame_buf[cnt].raw_val, touch_frame_buf[cnt].filter_val);
    }
    frame_msg_buf[len-1] = '}';
    frame_msg_buf[len] = '\r';
    frame_msg_buf[len+1] = '\n';
    return len + 2;
}

esp_err_t touch_tune_tool_write(void)
{
    uint32_t raw[TOUCH_PAD_MAX] = {0};
    uint32_t baseline[TOUCH_PAD_MAX] = {0}; 
    if(p_touch_tune_obj == NULL) {
        ESP_LOGW(TAG, "Touch tune tool invalid initialization ");
        return ESP_FAIL;
    }

    for(uint8_t pad = 0; pad < p_touch_tune_obj->channal_cnt; pad++) {
        p_touch_tune_obj->read_raw(p_touch_tune_obj->button[pad], &raw[pad]);
        p_touch_tune_obj->read_filtered(p_touch_tune_obj->button[pad], &baseline[pad]);
        p_touch_tune_obj->touch_data_list[pad].touch_num = p_touch_tune_obj->button[pad];
        p_touch_tune_obj->touch_data_list[pad].raw_val = raw[pad];
        p_touch_tune_obj->touch_data_list[pad].filter_val = baseline[pad];
    }
    int len = make_frame(p_touch_tune_obj->touch_data_list, p_touch_tune_obj->channal_cnt, p_touch_tune_obj->framemsg);
    touch_tune_push_msg(p_touch_tune_obj->touch_tune_uart_num, p_touch_tune_obj->framemsg, len);
    return ESP_OK;
}