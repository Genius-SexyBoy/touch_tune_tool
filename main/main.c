#include <stdio.h>
#include <stdlib.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "driver/touch_pad.h"
#include "esp_log.h"

#include "touch_tune_tool.h"

#define TOUCHPAD_FILTER_TOUCH_PERIOD (10)
#define TOUCH_THRESH_NO_USE   (0)
#define TOUCH_BUTTON_NUM    4

static const touch_pad_t button[TOUCH_BUTTON_NUM] = {
    TOUCH_PAD_NUM2,
    TOUCH_PAD_NUM3,
    TOUCH_PAD_NUM4, 
    TOUCH_PAD_NUM5,
};


void app_main()
{
    touch_pad_init();
    touch_pad_set_voltage(TOUCH_HVOLT_2V7, TOUCH_LVOLT_0V5, TOUCH_HVOLT_ATTEN_1V);
    for (int i = 0; i < TOUCH_BUTTON_NUM; i++) {
        touch_pad_config(button[i], TOUCH_THRESH_NO_USE);
    }
    touch_pad_filter_start(TOUCHPAD_FILTER_TOUCH_PERIOD);

    touch_tune_config_t touch_tune_config = {
        .button = button,
        .channal_cnt = TOUCH_BUTTON_NUM,
        .frame_push_type = SYSTEM_UART_PORT,
    };

    touch_tune_tool_init(&touch_tune_config);
    while(1) {
        touch_tune_tool_write();
        vTaskDelay(100 / portTICK_PERIOD_MS);
    }
    
}